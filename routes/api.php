<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/

Route::post('product', 'ProductController@createProduct');
Route::get('orders', 'OrderController@getOrders');
Route::get('orders/{type}', 'OrderController@getOrdersByProductType');
Route::post('order', 'OrderController@createOrder')->middleware('check.attempt');
