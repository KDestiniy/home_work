<?php

use App\Http\Controllers\ProductController;
use App\Model\Products;
use Illuminate\Http\Request;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ProductControllerTest extends \Tests\TestCase
{
    use DatabaseTransactions;

    protected function _before() {}
    protected function _after()
    {
        Mockery::close();
    }

    // tests
    public function testCreateProductSuccess()
    {
        $requestMock = Request::create('product', 'POST', [
            'price' => 1.20,
            'productType' => 'test',
            'color' => 'pink',
            'size' => '30x30x30',
        ]);

        $oProductController = new ProductController(new Products());
        $this->assertEquals(
            ['Success created'],
            $oProductController->createProduct($requestMock)->getData()
        );
    }

    public function testCreateProductFail()
    {
        $requestMock = Request::create('product', 'POST', []);

        $oProductController = new ProductController(new Products());
        $this->assertEquals(
            ['Product can`t be empty'],
            $oProductController->createProduct($requestMock)->getData()
        );
    }

    public function testHaveProduct()
    {
        $mock = $this->getMockBuilder(Products::class)->disableOriginalConstructor()->getMock();
        $mock->method('haveProduct')->willReturn(true);

        $requestMock = Request::create('product', 'POST', [
            'price' => 1.20,
            'productType' => 'test',
            'color' => 'pink',
            'size' => '30x30x30',
        ]);

        $oProductController = new ProductController($mock);
        $this->assertEquals(
            ['Product already exists'],
            $oProductController->createProduct($requestMock)->getData()
        );
    }

    public function testFailCreateProduct()
    {
        $mock = $this->getMockBuilder(Products::class)->disableOriginalConstructor()->getMock();
        $mock->method('save')->willReturn(false);

        $requestMock = Request::create('product', 'POST', [
            'price' => 1.20,
            'productType' => 'test',
            'color' => 'pink',
            'size' => '30x30x30',
        ]);

        $oProductController = new ProductController($mock);
        $this->assertEquals(
            ['Failed create'],
            $oProductController->createProduct($requestMock)->getData()
        );
    }
}
