<?php

use App\Http\Controllers\OrderController;
use App\Model\Orders;
use App\Model\Products;
use Codeception\Util\HttpCode;
use Illuminate\Http\Request;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class OrderControllerTest extends \Tests\TestCase
{
    use DatabaseTransactions;

    protected function _before() {}
    protected function _after() {}

    public function testGetOrders()
    {
        $orderMock = $this->getMockBuilder(Orders::class)->disableOriginalConstructor()->getMock();

        $orderController = new OrderController(new Products(), $orderMock);
        $result = $orderController->getOrders();

        $this->assertEquals(HttpCode::OK, $result->getStatusCode());
        $this->assertJson($result->getContent());
    }

    public function testGetOrdersByProductType()
    {
        $orderMock = $this->getMockBuilder(Orders::class)->disableOriginalConstructor()->getMock();

        $orderController = new OrderController(new Products(), $orderMock);
        $result = $orderController->getOrdersByProductType('someType');

        $this->assertEquals(HttpCode::OK, $result->getStatusCode());
        $this->assertJson($result->getContent());
    }

    public function testCreateOrderEmpty()
    {
        $orderMock = $this->getMockBuilder(Orders::class)->disableOriginalConstructor()->getMock();
        $requestMock = Request::create('order', 'POST', []);

        $orderController = new OrderController(new Products(), $orderMock);
        $result = $orderController->createOrder($requestMock);

        $this->assertEquals(HttpCode::OK, $result->getStatusCode());
        $this->assertEquals(['Order can`t be empty'], $result->getData());
    }

    public function testCreateOrderLimitExceeded()
    {
        $orderMock = $this->getMockBuilder(Orders::class)->disableOriginalConstructor()->getMock();
        $productMock = $this->getMockBuilder(Products::class)->disableOriginalConstructor()->getMock();
        $productMock->method('getProductTotalPrice')->willReturn(0);
        $requestMock = Request::create('order', 'POST', ['product' => ['Some fake data']]);

        $orderController = new OrderController($productMock, $orderMock);
        $result = $orderController->createOrder($requestMock);

        $this->assertEquals(HttpCode::OK, $result->getStatusCode());
        $this->assertEquals(['Minimum order limit not exceeded'], $result->getData());
    }

    public function testCreateOrderFail()
    {
        $orderMock = $this->getMockBuilder(Orders::class)->disableOriginalConstructor()->getMock();
        $productMock = $this->getMockBuilder(Products::class)->disableOriginalConstructor()->getMock();
        $productMock->method('getProductTotalPrice')->willReturn(1100);
        $productMock->method('save')->willReturn(false);
        $requestMock = Request::create('order', 'POST', ['product' => ['Some fake data']]);

        $orderController = new OrderController($productMock, $orderMock);
        $result = $orderController->createOrder($requestMock);

        $this->assertEquals(HttpCode::BAD_REQUEST, $result->getStatusCode());
        $this->assertEquals(['Failed create'], $result->getData());
    }

    public function testCreateOrderSuccess()
    {
        $orderMock = $this->getMockBuilder(Orders::class)->disableOriginalConstructor()->getMock();
        $orderMock->method('__get')->willReturn(1);
        $orderMock->method('save')->willReturn(true);
        $productMock = $this->getMockBuilder(Products::class)->disableOriginalConstructor()->getMock();
        $productMock->method('getProductTotalPrice')->willReturn(1100);
        $requestMock = Request::create('order', 'POST', ['product' => ['Some fake data']]);

        $orderController = new OrderController($productMock, $orderMock);
        $result = $orderController->createOrder($requestMock);

        $this->assertEquals(HttpCode::CREATED, $result->getStatusCode());
        $this->assertEquals(['Success created'], $result->getData());
    }
}
