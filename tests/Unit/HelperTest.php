<?php
/**
 * Created by PhpStorm.
 * User: Vitalik
 * Date: 05.02.2019
 * Time: 14:28
 */

namespace Tests\Unit;

use App\Helper;
use Illuminate\Support\Collection;

class HelperTest extends \Tests\TestCase
{
    protected function _before(){}
    protected function _after(){}

    /**
     * @param $data
     * @param $expected
     *
     * @dataProvider dataFloatProvider
     */
    public function testConvertFloatToMinor($data, $expected): void
    {
        $helper = new Helper();
        $actual = $helper->convertFloatToMinor($data['value'], $data['exponent']);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @param $data
     * @param $expected
     *
     * @dataProvider dataMinorProvider
     */
    public function testConvertMinorToFloat($data, $expected): void
    {
        $helper = new Helper();
        $actual = $helper->convertMinorToFloat($data['value'], $data['exponent']);

        $this->assertEquals($expected, $actual);
    }

    /**
     * @param $data
     * @param $type
     * @param $expected
     *
     * @dataProvider orderProvider
     */
    public function testConvertOrderData($data, $type, $expected)
    {
        $orders = $this->prepareOrderData($data);
        $helper = new Helper();

        $this->assertEquals($expected, $helper->convertOrderData($orders, $type));
    }

    public function orderProvider()
    {
        return [
            [
                [
                    [
                        'id' => 1,
                        'totalPrice' => 1018,
                        'countryCode' => 'US',
                        'quantity' => 1,
                        'price' => 108,
                        'productType' => 'testType',
                        'color' => 'black',
                        'size' => '30x30x30',
                    ],
                    [
                        'id' => 1,
                        'totalPrice' => 1018,
                        'countryCode' => 'US',
                        'quantity' => 1,
                        'price' => '910',
                        'productType' => 'testType',
                        'color' => 'red',
                        'size' => '32x32x32',
                    ],
                    [
                        'id' => 2,
                        'totalPrice' => 2500,
                        'countryCode' => 'LV',
                        'quantity' => 2,
                        'price' => 1000,
                        'productType' => 'test',
                        'color' => 'yellow',
                        'size' => '32x32x32',
                    ],
                    [
                        'id' => 2,
                        'totalPrice' => 2500,
                        'countryCode' => 'LV',
                        'quantity' => 1,
                        'price' => 500,
                        'productType' => 'test',
                        'color' => 'red',
                        'size' => '32x32x32',
                    ],
                ],
                '',
                [
                    [
                        'totalPrice' => '10.18',
                        'countryCode' => 'US',
                        'products' => [
                            [
                                'quantity' => 1,
                                'price' => '1.08',
                                'productType' => 'testType',
                                'color' => 'black',
                                'size' => '30x30x30',
                            ],
                            [
                                'quantity' => 1,
                                'price' => '9.10',
                                'productType' => 'testType',
                                'color' => 'red',
                                'size' => '32x32x32',
                            ],
                        ],
                    ],
                    [
                        'totalPrice' => '25.00',
                        'countryCode' => 'LV',
                        'products' => [
                            [
                                'quantity' => 2,
                                'price' => '10.00',
                                'productType' => 'test',
                                'color' => 'yellow',
                                'size' => '32x32x32',
                            ],
                            [
                                'quantity' => 2,
                                'price' => '5.00',
                                'productType' => 'test',
                                'color' => 'red',
                                'size' => '32x32x32',
                            ],
                        ],
                    ],
                ],
            ],
            [
                [
                    [
                        'id' => 1,
                        'totalPrice' => 1018,
                        'countryCode' => 'US',
                        'quantity' => 1,
                        'price' => 108,
                        'productType' => 'testType',
                        'color' => 'black',
                        'size' => '30x30x30',
                    ],
                    [
                        'id' => 1,
                        'totalPrice' => 1018,
                        'countryCode' => 'US',
                        'quantity' => 1,
                        'price' => '910',
                        'productType' => 'testType',
                        'color' => 'red',
                        'size' => '32x32x32',
                    ],
                    [
                        'id' => 2,
                        'totalPrice' => 2500,
                        'countryCode' => 'LV',
                        'quantity' => 2,
                        'price' => 1000,
                        'productType' => 'test',
                        'color' => 'yellow',
                        'size' => '32x32x32',
                    ],
                    [
                        'id' => 2,
                        'totalPrice' => 2500,
                        'countryCode' => 'LV',
                        'quantity' => 1,
                        'price' => 500,
                        'productType' => 'test',
                        'color' => 'red',
                        'size' => '32x32x32',
                    ],
                ],
                'test',
                [
                    [
                        'totalPrice' => '25.00',
                        'countryCode' => 'LV',
                        'products' => [
                            [
                                'quantity' => 2,
                                'price' => '10.00',
                                'productType' => 'test',
                                'color' => 'yellow',
                                'size' => '32x32x32',
                            ],
                            [
                                'quantity' => 2,
                                'price' => '5.00',
                                'productType' => 'test',
                                'color' => 'red',
                                'size' => '32x32x32',
                            ],
                        ],
                    ],
                ],
            ],
            [
                [],
                '',
                [],
            ]
        ];
    }

    /**
     * @return array
     */
    public function dataFloatProvider(): array
    {
        return [
            [
                [
                    'value' => 10.00,
                    'exponent' => 2,
                ],
                1000,
            ],
            [
                [
                    'value' => 10,
                    'exponent' => 2,
                ],
                10,
            ],
            [
                [
                    'value' => '10.000',
                    'exponent' => 3,
                ],
                10000,
            ],
        ];
    }

    /**
     * @return array
     */
    public function dataMinorProvider(): array
    {
        return [
            [
                [
                    'value' => 1000,
                    'exponent' => 2,
                ],
                10.00,
            ],
            [
                [
                    'value' => 1036,
                    'exponent' => 2,
                ],
                10.36,
            ],
            [
                [
                    'value' => '10000',
                    'exponent' => 3,
                ],
                10.000,
            ],
        ];
    }

    private function prepareOrderData($data)
    {
        $collection = Collection::make($data)->transform(function ($item) {
            $objectMock = new \stdClass();
            $objectMock->id = $item['id'];
            $objectMock->totalPrice = $item['totalPrice'];
            $objectMock->countryCode = $item['countryCode'];
            $objectMock->quantity = $item['id'];
            $objectMock->price = $item['price'];
            $objectMock->productType = $item['productType'];
            $objectMock->color = $item['color'];
            $objectMock->size = $item['size'];

            return $objectMock;
        });

        return $collection;
    }
}
