<?php

use Codeception\Util\HttpCode;

class ProductCest
{
    private $product = [
        'price' => 30.02,
        'productType' => 'test Type product',
        'color' => 'red',
        'size' => '30x30x30',
    ];

    public function _before(ApiTester $I) {}

    // tests
    public function createSuccessProduct(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('product', $this->product);
        $I->seeResponseCodeIs(HttpCode::CREATED);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['Success created']);
    }

    public function createProductWrongUrl(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('products', ['someData']);
        $I->seeResponseCodeIs(HttpCode::NOT_FOUND);
    }

    public function createProductEmpty(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('product', []);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['Product can`t be empty']);
    }

    public function createProductAlreadyExists(ApiTester $I)
    {
        $I->haveRecord('Products', $this->product);
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('product', $this->product);
        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['Product already exists']);
    }
}
