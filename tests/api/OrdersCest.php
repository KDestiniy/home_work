<?php

use Codeception\Util\HttpCode;

class OrdersCest
{
    public function _before(ApiTester $I) {}

    protected $orderData = [
        'product' => [
            [
                'id' => 1,
                'quantity' => 3,
            ],
            [
                'id' => 2,
                'quantity' => 1,
            ]
        ]
    ];

    // tests
    public function createOrderSuccess(ApiTester $I)
    {
        $I->haveRecord('Products', [
            'id' => 1,
            'price' => 3002,
            'productType' => 'test Type product',
            'color' => 'red',
            'size' => '30x30x30',
        ]);

        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('order', $this->orderData);

        $I->seeResponseCodeIs(HttpCode::CREATED);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['Success created']);
    }

    public function createOrderEmpty(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('order', []);

        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['Order can`t be empty']);
    }

    public function createOrderLimitExceeded(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('order', $this->orderData);

        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson(['Minimum order limit not exceeded']);
    }

    public function createOrderNotValidUrl(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPOST('not_valid_url', []);

        $I->seeResponseCodeIs(HttpCode::NOT_FOUND);
    }

    public function getOrders(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGET('orders');

        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([]);
    }

    public function getOrdersByType(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendGET('orders/test');

        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseContainsJson([]);
    }
}
