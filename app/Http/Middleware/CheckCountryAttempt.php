<?php

namespace App\Http\Middleware;

use Closure;
use Ircop\Antiflood\Antiflood;

class CheckCountryAttempt
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $countryCode = geoip($request->ip())->iso_code;
        $antifoold = new Antiflood();

        if ($antifoold->check($countryCode, env('MAX_ATTEMPTS', 10)) === false) {
            return response()->json(['For this country attempts is exceeded try later.']);
        }

        $antifoold->put($countryCode, env('REFRESH_TIMEOUT', 5));
        return $next($request);
    }
}
