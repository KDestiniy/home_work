<?php

namespace App\Http\Controllers;

use App\Helper;
use App\Model\Products;
use Codeception\Util\HttpCode;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    const EXPONENT = 2;

    protected $products;

    public function __construct(Products $products)
    {
        $this->products = $products;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function createProduct(Request $request) :JsonResponse
    {
        $price = (new Helper)->convertFloatToMinor($request->input('price', 0), self::EXPONENT);
        $type = $request->input('productType', '');
        $color = $request->input('color', '');
        $size = $request->input('size', '');

        if (empty($price) && empty($type) && empty($color) && empty($size)) {
            return response()->json(['Product can`t be empty']);
        }

        if ($this->products->haveProduct($type, $color, $size)) {
            return response()->json(['Product already exists']);
        }

        $this->products->price = $price;
        $this->products->productType = $type;
        $this->products->color = $color;
        $this->products->size = $size;

        $isSave = $this->products->save();

        $response = $isSave ? [['Success created'], HttpCode::CREATED] : [['Failed create'], HttpCode::BAD_REQUEST];
        return response()->json(...$response);
    }
}
