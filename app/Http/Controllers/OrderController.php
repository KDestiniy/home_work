<?php

namespace App\Http\Controllers;

use App\Helper;
use App\Model\Orders;
use App\Model\OrdersLinks;
use App\Model\Products;
use Codeception\Util\HttpCode;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    // price in minor unit
    const TOTAL_PRICE = 1000;

    protected $products;
    protected $orders;

    public function __construct(Products $products, Orders $orders)
    {
        $this->products = $products;
        $this->orders = $orders;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function createOrder(Request $request) :JsonResponse
    {
        $orderData = $request->input('product', []);

        if (empty($orderData)) {
            return response()->json(['Order can`t be empty']);
        }

        $totalPrice = $this->products->getProductTotalPrice($orderData);

        if ($totalPrice < self::TOTAL_PRICE) {
            return response()->json(['Minimum order limit not exceeded']);
        }

        $this->orders->totalPrice = $totalPrice;
        $this->orders->countryCode = geoip($request->ip())->iso_code;

        if (!$this->orders->save()) {
            return response()->json(['Failed create'], HttpCode::BAD_REQUEST);
        }

        (new OrdersLinks())->saveLinkOrder($orderData, $this->orders->id);

        return response()->json(['Success created'], HttpCode::CREATED);
    }

    /**
     * @return JsonResponse
     */
    public function getOrders() :JsonResponse
    {
        $helper = new Helper();
        $allOrders = $this->orders->getAllOrders();

        return response()->json($helper->convertOrderData($allOrders));
    }

    /**
     * @param string $type
     * @return JsonResponse
     */
    public function getOrdersByProductType(string $type) :JsonResponse
    {
        $helper = new Helper();
        $allOrders = $this->orders->getAllOrders();

        return response()->json($helper->convertOrderData($allOrders, $type));
    }
}
