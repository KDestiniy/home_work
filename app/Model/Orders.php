<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

/**
 * App\Model\Orders
 *
 * @property int $id
 * @property int $totalPrice
 * @property string $countryCode
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Orders newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Orders newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Orders query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Orders whereCountryCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Orders whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Orders whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Orders whereTotalPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Orders whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Orders extends Model
{
    public function getAllOrders() :Collection
    {
        return DB::table('orders')
            ->join('orders_links', 'orders.id', '=', 'orders_links.orderId')
            ->join('products', 'orders_links.productId', '=', 'products.id')
            ->select(
                'orders.id',
                'orders.totalPrice',
                'orders.countryCode',
                'orders_links.quantity',
                'products.price',
                'products.productType',
                'products.color',
                'products.size'
            )->get();
    }
}
