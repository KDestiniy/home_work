<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * App\Model\Products
 *
 * @property int $id
 * @property int $price
 * @property string $productType
 * @property string $color
 * @property string $size
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Products newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Products newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Products query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Products whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Products whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Products whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Products wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Products whereProductType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Products whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\Products whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Products extends Model
{
    /**
     * Check product by credential
     *
     * @param string $type
     * @param string $color
     * @param string $size
     * @return bool
     */
    public function haveProduct(string $type, string $color, string $size) :bool
    {
        $product = DB::table('products')
            ->where('productType', $type)
            ->where('color', $color)
            ->where('size', $size)
            ->first();

        return $product ? true : false;
    }

    public function getProductTotalPrice(array $orderData) :int
    {
        $products = DB::table('products')
            ->whereIn('id', array_column($orderData, 'id'))
            ->get()
            ->toArray();

        $totalPrice = 0;
        array_walk($products, function($item) use (&$totalPrice, $orderData) {
            $orderId = array_search($item->id, array_column($orderData, 'id'));
            $order = $orderData[$orderId];
            $totalPrice = $item->price * $order['quantity'] + $totalPrice;
        });

        return $totalPrice;
    }
}
