<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

/**
 * App\Model\OrdersLinks
 *
 * @property int $id
 * @property int $orderId
 * @property int $productId
 * @property int $quantity
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\OrdersLinks newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\OrdersLinks newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\OrdersLinks query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\OrdersLinks whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\OrdersLinks whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\OrdersLinks whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\OrdersLinks whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\OrdersLinks whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Model\OrdersLinks whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class OrdersLinks extends Model
{
    public function saveLinkOrder(array $orderData, int $id) :void
    {
        $insertData = [];
        foreach ($orderData as $item) {
            if (isset($item['id']) && isset($item['quantity'])) {
                $insertData[] = [
                    'orderId' => $id,
                    'productId' => (int)$item['id'],
                    'quantity' => (int)$item['quantity']
                ];
            }
        }

        if (!empty($insertData)) {
            DB::table('orders_links')->insert($insertData);
        }
    }
}
