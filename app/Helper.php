<?php

namespace App;


use App\Http\Controllers\ProductController;
use App\Model\Orders;
use Illuminate\Support\Collection;

class Helper
{
    /**
     * @param $value
     * @param $exp
     * @return float|int
     */
    public function convertFloatToMinor($value, $exp)
    {
        if (is_int($value)) {
            return $value;
        }

        return $value * pow(10, $exp);
    }

    /**
     * @param $value
     * @param $exp
     * @return float|int
     */
    public function convertMinorToFloat($value, $exp)
    {
        return number_format($value / pow(10, $exp), $exp);
    }

    /**
     * @param Collection $orders
     * @param string $type
     * @return array
     */
    public function convertOrderData(Collection $orders, string $type = '') :array
    {
        $returnData = [];
        if (count($orders) !== 0) {
            foreach ($orders as $order) {
                if (!empty($type) && empty($orders->where('id', $order->id)->where('productType', $type)->toArray())) {
                    continue;
                }

                if (!isset($returnData[$order->id]['totalPrice'])) {
                    $returnData[$order->id]['totalPrice'] = $this->convertMinorToFloat($order->totalPrice, ProductController::EXPONENT);
                    $returnData[$order->id]['countryCode'] = $order->countryCode;
                }

                $returnData[$order->id]['products'][] = [
                    'quantity' => $order->quantity,
                    'price' => $this->convertMinorToFloat($order->price, ProductController::EXPONENT),
                    'productType' => $order->productType,
                    'color' => $order->color,
                    'size' => $order->size,
                ];
            }
        }

        return array_values($returnData);
    }
}
