## Install project
Create database in mysql using phpMyadmin or other interface.

Find file in root dirictory .env.example and rename it to .env. Set your own settings in config.

Run command ``composer install``

##Start project
Just start app ``php artisan serve``

##Tests
To run tests enter command ``vendor\bin\codecept run --steps``

##API endpoints
example ``curl -H "Content-Type: application/json" -X POST -d '{"price":11.20,"productType":"test","color":"red","size":"302x320x302"}' http://localhost:8000/api/product``

Method POST  ``api/product`` 

{"price":1120,"productType":"test","color":"red","size":"302x320x302"}

Method GET  ``api/orders`` 

Method GET  ``api/orders/{type}`` 

Method POST  ``api/order`` 

{"id":1,"quantity":"1"}
