<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersLinksTable extends Migration
{
    protected $fillable = ['orderId', 'productId', 'quantity'];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders_links', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('orderId');
            $table->integer('productId');
            $table->integer('quantity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_links');
    }
}
