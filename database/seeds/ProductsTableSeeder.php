<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $products = [
            [
                'price' => 1020,
                'productType' => 'test',
                'color' => 'black',
                'size' => '30x30x30',
            ],
            [
                'price' => 1120,
                'productType' => 'test',
                'color' => 'red',
                'size' => '302x320x302',
            ]
        ];

        foreach ($products as $product) {
            DB::table('products')->insert($product);
        }

    }
}
